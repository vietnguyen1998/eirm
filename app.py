import os
import urllib.request
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
import numpy as np
import os
from keras.models import model_from_json
# from predict import reStoreModel
import predict
import encode

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

app = Flask(__name__)
UPLOAD_FOLDER = 'static/img'
global imgOriginUploaded
global imgArnoldEncryption
global imgLogisticEncryption

global imgOriginUploaded_Des
global imgArnoldDecryption
global imgLogisticDecryption

loadModel = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['modelUploaded'] = False
app.secret_key = 'vclluoon'
app.config['SESSION_TYPE'] = 'filesystem'
# model = pickle.load(open('model.h5', 'rb'))
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

@app.route('/encryption', methods=['GET', 'POST'])
def upload_image():
    global imgUploaded
    global imgEncription
    global imgDecription_catmap
    if request.method == 'GET' or request.method == 'POST':
        if 'file' not in request.files:
            return render_template('a.html')
        file = request.files['file']
        if file.filename == '':
            flash('No image selected for uploading')
            return render_template('index.html')
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            imgUploaded = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(imgUploaded)
            text = request.form['pword_original']
            print(text)
            img, imgEncription = encode.LogisticEncryption(imgUploaded, text)
            return render_template('a.html', imgName=imgEncription, imgUploaded=imgUploaded)
        else:
            flash('Allowed image types are -> png, jpg, jpeg, gif')
            return render_template('a.html')
    else:
        return render_template('a.html')

@app.route('/decryption', methods=['GET', 'POST'])
def decriptionImage():
    global imgUploaded
    global imgEncription
    global imgDecription_catmap
    if request.method == 'GET' or request.method == 'POST':
        text = request.form['pword_decryption']
        print("imgEncription")
        print(text)
        print(imgEncription)
        img2, imgDecription = encode.LogisticDecryption('static/img/example_LogisticEnc.png', text)

        return render_template('a.html', imgName="static/img/example_LogisticEnc.png", imgUploaded="static/img/example.jpeg", imgDecription=imgDecription)
    else:
        return render_template('a.html')

@app.route('/encryption_catmap', methods=['GET', 'POST'])
def upload_image_catmap():
    global imgOriginUploaded
    global imgArnoldEncryption
    global imgLogisticEncryption

    if request.method == 'GET' or request.method == 'POST':
        if 'file' not in request.files:
            return render_template('a.html', message='Bạn chưa tải lên ảnh')
        file = request.files['file']
        if file.filename == '':
            flash('No image selected for uploading')
            return render_template('a.html', message='Bạn chưa tải lên ảnh')
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            imgUploaded = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(imgUploaded)
            loop = request.form['loop']
            password = request.form['password']
            control = request.form['control']
            print(loop)

            img, imgName, imgEncryptXOR, imgNameEncryptXOR = encode.ArnoldCatEncryption(imgUploaded, loop, password, control)
            # LogisticEncryption(imgUploaded, text)
            imgOriginUploaded = imgUploaded
            imgArnoldEncryption = imgName
            imgLogisticEncryption = imgNameEncryptXOR
            return render_template('encode.html', image_origin=imgUploaded, image_mix=imgName, image_encryption = imgNameEncryptXOR)
        else:
            return render_template('a.html', message='File tải lên không phải ảnh')
    else:
        return render_template('a.html', message='Phương thức không hợp lệ')

@app.route('/decryption_catmap', methods=['GET', 'POST'])
def decriptionImage_catmap():

    global imgOriginUploaded_Des
    global imgArnoldDecryption
    global imgLogisticDecryption

    if request.method == 'GET' or request.method == 'POST':
        if 'file' not in request.files:
            return render_template('a.html', message='Bạn chưa tải lên ảnh')
        file = request.files['file']
        if file.filename == '':
            flash('No image selected for uploading')
            return render_template('a.html', message='Bạn chưa tải lên ảnh')
        if file and allowed_file(file.filename):

            filename = secure_filename(file.filename)
            imgUploaded = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(imgUploaded)
            loop = request.form['loop']
            password = request.form['password']
            control = request.form['control']
            imgNameDecLogistic, imgDecLogistic, imgNameArnold, imgArnold = encode.ArnoldCatDecryption(imgUploaded, loop, password, control)

            imgOriginUploaded_Des = imgUploaded
            imgArnoldDecryption = imgNameDecLogistic
            imgLogisticDecryption = imgNameArnold

        return render_template('decode.html', image_encryption=imgUploaded, image_mix=imgNameDecLogistic, image_origin=imgNameArnold)
    else:
        return render_template('a.html', message='File tải lên không phải ảnh')

@app.route('/encryption_histogram', methods=['GET', 'POST'])
def encryptionHistogram():
    print(imgOriginUploaded)
    print(imgArnoldEncryption)
    print(imgLogisticEncryption)

    histogram_origin = encode.create_plt(imgOriginUploaded)
    histogram_ArnoldEncryption = encode.create_plt(imgArnoldEncryption)
    histogram_LogisticEncryption = encode.create_plt(imgLogisticEncryption)

    return render_template('encode.html', histogram_origin=histogram_origin, histogram_ArnoldEncryption=histogram_ArnoldEncryption,histogram_LogisticEncryption=histogram_LogisticEncryption, image_origin=imgOriginUploaded, image_mix=imgArnoldEncryption, image_encryption = imgLogisticEncryption)

@app.route('/decryption_histogram', methods=['GET', 'POST'])
def decryptionHistogram():

    print(imgOriginUploaded_Des)
    print(imgArnoldDecryption)
    print(imgLogisticDecryption)

    histogram_LogisticDecryption = encode.create_plt(imgOriginUploaded_Des)
    histogram_ArnoldDecryption = encode.create_plt(imgArnoldDecryption)
    histogram_origin_de = encode.create_plt(imgLogisticDecryption)

    return render_template('decode.html', histogram_LogisticDecryption=histogram_LogisticDecryption, histogram_ArnoldDecryption=histogram_ArnoldDecryption,histogram_origin_de=histogram_origin_de, image_origin=imgLogisticDecryption, image_mix=imgArnoldDecryption, image_encryption = imgOriginUploaded_Des)

@app.route('/decode', methods=['GET', 'POST'])
def logisticChaoMap():
    return render_template('decode.html')

@app.route('/encode', methods=['GET', 'POST'])
def catmap():
    return render_template('encode.html')

@app.route('/',methods=['GET'])
def home():
    return render_template('home.html')


if __name__ == "__main__":
    app.run(debug=True)
