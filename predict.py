import os
import urllib.request
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
import numpy as np
import os
import scipy.misc as mi
from keras.models import model_from_json

# import app
app = Flask(__name__)
# model = False
app.config['modelUploaded'] = False

def reStoreModel():
    
    # load json and create model
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    if app.config['modelUploaded'] == False:
        loaded_model.load_weights("model.h5")
        app.config['modelUploaded'] = True
    print("loaded model from disk")
    return loaded_model


def processData(direction, typeS):
    input_img = cv2.imread(direction)
    gray = cv2.cvtColor(input_img,cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,50,150,apertureSize = 3)
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, minLineLength=40, maxLineGap=1)
    arr_lines = []
    for x in lines:
        for y in x:
            arr_lines.append([y[0], y[1], y[2], y[3]])
    lines_x = []
    lines_y = []
    for x in arr_lines:
        if abs(x[0] - x[2]) < 5:
            lines_x.append(x)
        else:
            lines_y.append(x)
    lines_x.sort()
    lines_y = sorted(lines_y ,key=lambda x: x[1])
    for x in edges:
        length_x = len(x)
        break
    for y in edges.T:
        length_y = len(y)
        break
    lines_x1 = []
    x1 = lines_x[1][0]
    x_min = lines_x[0][0]
    x_max = lines_x[0][2]
    for i in range(0, len(lines_x) - 2):
        if x1 - lines_x[i][0] < 5:
            x1 = lines_x[i + 2][0]
            if i == len(lines_x) - 3:
                x_max = lines_x[i + 2][2]
                lines_x1.append([x_min, 0, x_max, length_y])
            continue
        else:
            x_max = lines_x[i][2]
            lines_x1.append([x_min, 0, x_max, length_y])
            x1 = lines_x[i+2][0]
            x_min = lines_x[i + 1][0]
            x_max = lines_x[i + 1][2]            
    lines_y1 = []
    j = 0
    y1 = lines_y[1][1]
    for i in range(0, len(lines_y) - 2):
        if y1 - lines_y[i][1] < 5:
            y1 = lines_y[i + 2][1]
            if i == len(lines_y) - 3:
                x = 0
                y = 0
                z = 0
                t = 0
                for k in range(j, i + 1):
                    x += lines_y[k][0]
                    y += lines_y[k][1]
                    z += lines_y[k][2]
                    t += lines_y[k][3]
                x_avg = int(x / (i + 1 - j))
                y_avg = int(y / (i + 1 - j))
                z_avg = int(z / (i + 1 - j))
                t_avg = int(t / (i + 1 - j))
                lines_y1.append([x_avg, y_avg, z_avg, t_avg])
            continue
        else:
            x = 0
            y = 0
            z = 0
            t = 0
            for k in range(j, i + 1):
                x += lines_y[k][0]
                y += lines_y[k][1]
                z += lines_y[k][2]
                t += lines_y[k][3]
            if i == j :
                x_avg = lines_y[i][0]
                y_avg = lines_y[i][1]
                z_avg = lines_y[i][2]
                t_avg = lines_y[i][3]
            else:
                x_avg = int(x / (i + 1 - j))
                y_avg = int(y / (i + 1 - j))
                z_avg = int(z / (i + 1 - j))
                t_avg = int(t / (i + 1 - j))
            lines_y1.append([x_avg, y_avg, z_avg, t_avg])
            j = i + 1
            y1 = lines_y[i+2][1]
    
    lines_y2 = []
    for i in range(0, len(lines_y1) - 1):
        if lines_y1[i+1][1] - lines_y1[i][1] < 50:
            lines_y2.append(lines_y1[i])
        if i == len(lines_y1) - 2:
            lines_y2.append(lines_y1[i + 1])
    lines_y1.clear()
    lines_y1 = lines_y2
    lines_y1.remove(lines_y1[0])
    lines_x.clear()
    lines_y.clear()
    lines_y = lines_y1
#     toạ độ nơi cắt
    if typeS == 1:
        lines_x.append(lines_x1[1])
        lines_x.append(lines_x1[2])
    if typeS == 2:
        lines_x.append(lines_x1[4])
        lines_x.append(lines_x1[5])
    if typeS == 3:
        lines_x.append(lines_x1[3])
        lines_x.append(lines_x1[4])


#     print(lines_x)
    
    lines_x1 = []
    for x in lines_x:
        lines_x1.append(x)
            
    lines_y1 = []
    for y in lines_y:
        if y[1] == y[3]:
            lines_y1.append([0, y[1], length_x, y[1]])
        else:
            
            a = (y[1] - y[3]) / (y[0] - y[2])
            b = (y[0] * y[3] - y[1] * y[2]) / (y[0] - y[2])
            lines_y1.append([0, int(b), length_x, int(a*length_x + b)])
    
    for x in lines_x1:
        cv2.line(input_img, (x[0],x[1]), (x[2],x[3]), (0,0,255), 1)
    for y in lines_y1:
        cv2.line(input_img, (y[0],y[1]), (y[2], y[3]), (0,0,255), 1)
    
    if typeS == 1:
        cv2.imwrite('/Users/nguyenviet/Desktop/CNN_DA/upload/input/pic2.jpg',input_img)
    if typeS == 2:
        cv2.imwrite('/Users/nguyenviet/Desktop/CNN_DA/upload/input/pic1.jpg',input_img)
    if typeS == 3:
        cv2.imwrite('/Users/nguyenviet/Desktop/CNN_DA/upload/input/pic3.jpg',input_img)

    
    coordinate1 = []
    coordinate2 = []
    i = 0
    for x in lines_x:
        if x[0] == x[2]:
            x1 = x[0]
            for y in lines_y:
                if y[1] == y[3]:
                    y1 = y[1]
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
                else:
                    a1 = (y[1] - y[3]) / (y[0] - y[2])
                    b1 = (y[0] * y[3] - y[1] * y[2]) / (y[0] - y[2])
                    y1 = int(a1 * x1 + b1)
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
        else:
            for y in lines_y:
                if y[1] == y[3]:
                    y1 = y[1]
                    a1 = (x[1] - x[3]) / (x[0] - x[2])
                    b1 = (x[0] * x[3] - x[1] * x[2]) / (x[0] - x[2])
                    x1 = int((y1 - b1) / a1)
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
                else:
                    a1 = (x[1] - x[3]) / (x[0] - x[2])
                    b1 = (x[0] * x[3] - x[1] * x[2]) / (x[0] - x[2])
                    a2 = (y[1] - y[3]) / (y[0] - y[2])
                    b2 = (y[0] * y[3] - y[1] * y[2]) / (y[0] - y[2])
                    x1 = int((b2 - b1) / (a1 - a2))
                    y1 = int((a1 * b2 - a2 * b1) / (a1 - a2))
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
        i += 1
    
    for y in reversed(coordinate1):
        coordinate1.remove(y)
        break
    for x in coordinate2:
        coordinate2.remove(x)
        break
    for x in coordinate1:
        t = x[1] + 2
        x[1] = t
    for x in coordinate2:
        t = x[1] + 2
        x[1] = t
        
    coordinates = []
    i = 0
    for x in coordinate1:
        coor = []
        coor.append(x[0])
        coor.append(x[1])
        j = 0
        for y in coordinate2:
            if j == i:
                coor.append(y[0])
                coor.append(y[1])
            j += 1
        i += 1
        coordinates.append(coor)
        
    return coordinates  

def getDigit1(image, digit_name, stt):
    test = np.asarray(image)
    test.setflags(write=1)
    for x in test:
        i = 0
        for y in x:
            t = 255 - y
            x[i] = t
            i += 1
    for x in test:
        i = 0
        for y in x:
            if y < 40:
                x[i] = 0
            i += 1
    test[0] = 0
    test[1] = 0
    test[2] = 0
    test[3] = 0
    test_width = len(test[0])
    for column in test.T:
        test_weight = len(column)
        break
    test[test_weight - 1] = 100
    for x in test:
        i = 0
        x[0] = 0
        x[1] = 0
        x[2] = 0
        x[3] = 0
        x[4] = 0
        x[5] = 0
        x[len(x)-1] = 0
        x[len(x)-2] = 0
        x[len(x)-3] = 0
        x[len(x)-4] = 0
        x[len(x)-5] = 0
        i += 1
        
    # xoa duong ke ngang
# =============================================================================
#     i = 0
#     for x in test:
#         if i == test_weight - 2:
#             x[3] = 255
#             break
#         i += 1
# =============================================================================
    arr = []
    n = 0
    for x in test:
        if n < test_weight - 3:
            arr.clear()
            for y in x:
                arr.append(y)
            n += 1
            continue
        i = 3
        j = 0
        if x[5] > 30 or x[6] > 30 or x[7] > 30 or x[8] > 30 or x[9] > 30:
            a = 0
            for y in x:
                if a < 40:
                    x[i] = 0
                    arr[i] = 0
                else:
                    arr[i] = x[i]
                    break
#                 print(arr)
                i += 1
                if i > len(x) - 1:
                    break
                else:
                    a = arr[i]
#                     print(a)
            k = len(x) - 3
            for y in x:
                if arr[k - j] < 40:
                    x[k - j] = 0
                    arr[k - j] = 0
                else:
                    arr[k - j] = x[k - j]
                    break
#                 print(arr)
                j += 1
        else:
            arr.clear()
            for y in x:
                arr.append(y)
                i += 1
    
    # tinh kich thuoc so
    pixel_a = 0
    i = 0
    for column in test.T:
        j = 0
        for x in reversed(column):
            if j > len(column) - 10:
                break
            if x > 30:
                pixel_a = i
                break
            j += 1
        if pixel_a > 0:
            break
        i += 1
    digit_width = 0
    i = 0
    for column in test.T:
        if i < pixel_a + 13:
            i += 1
            continue
        j = 0
        for x in column:
            if j < 3:
                j += 1
                continue
            if j == len(column) - 12:
                break
            if x == 0:
                check = 1
                j += 1
                continue
            else:
                check = 0
                break
        if check == 1:
            digit_width = 8
            break
        else:
            digit_width = 13
            break
        i += 1
    pixel_b = 0
    i = 0
    for column in test.T:
        if i < pixel_a + digit_width:
            i += 1
            continue
        j = 0
        for x in column:
            if j < 3:
                j += 1
                continue
            if j == len(column) - 13:
                break
            if x == 0:
                check = 1
                j += 1
                continue
            else:
                check = 0
                break
        if check == 1:
            pixel_b = i
            break
        i += 1
    # pixel_b += 1
    if pixel_b > pixel_a + 30:
        pixel_b = pixel_a + 20
        
    # get digit
    # digit1 = np.empty((0,20), int)
    digit1 = []
    i = 0
    k = 1
    for x in test:
        if i < 3:
            i += 1
            continue
        j = 0
        if i > test_weight - 3:
            # arr1[pixel_b - pixel_a + 1] = 0
            x[pixel_b] = 0
            x[pixel_b - 1] = 0
        arr1 = []
        for y in x:
            if j < pixel_a - 3:
                j += 1
                continue
            arr1.append(y)
            j += 1
            if j == pixel_b:
                arr1.append(0)
                arr1.append(0)
                arr1.append(0)
                break
        digit1.append(arr1)
        # digit1 = np.append(digit1, arr1[0], axis=0)
        i += 1
    arr2 = []
    for x in arr1:
        arr2.append(0)
    digit1.append(arr2)
    digit1.append(arr2)
    # imageio.imwrite('input/digit1.jpg', np.asarray(digit1))
    mi.imsave('/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/digit1.jpg', digit1)
#     print(digit1)
    dir = "/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/" + str(digit_name)
    if not os.path.exists(dir):
        os.makedirs(dir)
    save_location = dir + "/" + str(stt) + "a.jpg"
    mi.imsave(save_location, digit1)

def getDigit2(image, digit_name, stt):
    test = np.asarray(image)
    test.setflags(write=1)
    for x in test:
        i = 0
        for y in x:
            t = 255 - y
            x[i] = t
            i += 1
    for x in test:
        i = 0
        for y in x:
            if y < 40:
                x[i] = 0
            i += 1
    test[0] = 0
    test[1] = 0
    test[2] = 0
    test[3] = 0
    test_width = len(test[0])
    for column in test.T:
        test_weight = len(column)
        break
    test[test_weight - 1] = 255
    for x in test:
        i = 0
        x[0] = 0
        x[1] = 0
        x[2] = 0
        x[len(x)-1] = 0
        x[len(x)-2] = 0
        x[len(x)-3] = 0
        i += 1
    
    # xoa duong ke ngang
    arr = []
    for x in test:
        i = 3
        j = 0
        if x[3] > 30 or x[4] > 30 or x[5] > 30:
            a = 0
            for y in x:
                if a < 40:
                    x[i] = 0
                    arr[i] = 0
                else:
                    arr[i] = x[i]
                    break
                # print(arr)
                i += 1
                if i > len(x) - 1:
                    break
                else:
                    a = arr[i]
                    # print(a)
            k = len(x) - 3
            for y in x:
                if arr[k - j] < 40:
                    x[k - j] = 0
                    arr[k - j] = 0
                else:
                    arr[k - j] = x[k - j]
                    break
                # print(arr)
                j += 1
        else:
            arr.clear()
            for y in x:
                arr.append(y)
                i += 1
    
    # tinh kich thuoc so
    pixel_a = 0
    i = 0
    for column in reversed(test.T):
        j = 0
        for x in column:
            if j > len(column) - 5:
                break
            if x > 30:
                pixel_a = i
                break
            j += 1
        if pixel_a > 0:
            break
        i += 1
    digit_width = 0
    i = 0
    for column in reversed(test.T):
        if i < pixel_a + 13:
            i += 1
            continue
        j = 0
        for x in column:
            if j < 3:
                j += 1
                continue
            if j == len(column) - 12:
                break
            if x == 0:
                check = 1
                j += 1
                continue
            else:
                check = 0
                break
        if check == 1:
            digit_width = 5
            break
        else:
            digit_width = 13
            break
        i += 1
    pixel_b = 0
    i = 0
    for column in reversed(test.T):
        if i < pixel_a + digit_width:
            i += 1
            continue
        j = 0
        for x in column:
            if j < 3:
                j += 1
                continue
            if j == len(column) - 10:
                break
            if x == 0:
                check = 1
                j += 1
                continue
            else:
                check = 0
                break
        if check == 1:
            pixel_b = i
            break
        i += 1
    if pixel_b > pixel_a + 30:
        pixel_b = pixel_a + 20
        
    # get digit2
    digit2 = []
    i = 0
    k = 1
    for x in test:
        if i < 3:
            i += 1
            continue
        j = 0
        arr1 = []
        for y in reversed(x):
            if j < pixel_a - 2:
                j += 1
                continue
            arr1.append(y)
            j += 1
            if j == pixel_b:
                arr1.append(0)
                arr1.append(0)
                arr1.append(0)
                break
# =============================================================================
#         if i > test_weight - 4:
#             arr1[pixel_b - pixel_a + 2] = 0
# =============================================================================
        digit2.append(arr1)
        i += 1
    arr2 = []
    for x in arr1:
        arr2.append(0)
    digit2.append(arr2)
    digit2.append(arr2)
    # imageio.imwrite('input/digit2.jpg', np.asarray(digit))
    mi.imsave('/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/digit2.jpg', digit2)
    # fix digit2  
    digit2_img = Image.open('/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/digit2.jpg').convert('L')
    digit2_arr = np.asarray(digit2_img)
    digit2_arr.setflags(write=1)
    digit2_arr_new = []
    for x in digit2_arr:
        arr2 = []
        for y in reversed(x):
            arr2.append(y)
        digit2_arr_new.append(arr2)
    mi.imsave('/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/digit2.jpg', digit2_arr_new)
    dir = "/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/" + str(digit_name)
    if not os.path.exists(dir):
        os.makedirs(dir)
    save_location = dir + "/" + str(stt) + "b.jpg"
    mi.imsave(save_location, digit2_arr_new)


def processData_malopthi(direction):
    input_img = cv2.imread(direction)
    gray = cv2.cvtColor(input_img,cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,50,150,apertureSize = 3)
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, minLineLength=40, maxLineGap=1)
    arr_lines = []
    for x in lines:
        for y in x:
            arr_lines.append([y[0], y[1], y[2], y[3]])
    lines_x = []
    lines_y = []
    for x in arr_lines:
        if abs(x[0] - x[2]) < 5:
            lines_x.append(x)
        else:
            lines_y.append(x)
    lines_x.sort()
    lines_y = sorted(lines_y ,key=lambda x: x[1])
    for x in edges:
        length_x = len(x)
        break
    for y in edges.T:
        length_y = len(y)
        break
    lines_x1 = []
    x1 = lines_x[1][0]
    x_min = lines_x[0][0]
    x_max = lines_x[0][2]
    for i in range(0, len(lines_x) - 2):
        if x1 - lines_x[i][0] < 5:
            x1 = lines_x[i + 2][0]
            if i == len(lines_x) - 3:
                x_max = lines_x[i + 2][2]
                lines_x1.append([x_min, 0, x_max, length_y])
            continue
        else:
            x_max = lines_x[i][2]
            lines_x1.append([x_min, 0, x_max, length_y])
            x1 = lines_x[i+2][0]
            x_min = lines_x[i + 1][0]
            x_max = lines_x[i + 1][2]            
    lines_y1 = []
    j = 0
    y1 = lines_y[1][1]
    for i in range(0, len(lines_y) - 2):
        if y1 - lines_y[i][1] < 5:
            y1 = lines_y[i + 2][1]
            if i == len(lines_y) - 3:
                x = 0
                y = 0
                z = 0
                t = 0
                for k in range(j, i + 1):
                    x += lines_y[k][0]
                    y += lines_y[k][1]
                    z += lines_y[k][2]
                    t += lines_y[k][3]
                x_avg = int(x / (i + 1 - j))
                y_avg = int(y / (i + 1 - j))
                z_avg = int(z / (i + 1 - j))
                t_avg = int(t / (i + 1 - j))
                lines_y1.append([x_avg, y_avg, z_avg, t_avg])
            continue
        else:
            x = 0
            y = 0
            z = 0
            t = 0
            for k in range(j, i + 1):
                x += lines_y[k][0]
                y += lines_y[k][1]
                z += lines_y[k][2]
                t += lines_y[k][3]
            if i == j :
                x_avg = lines_y[i][0]
                y_avg = lines_y[i][1]
                z_avg = lines_y[i][2]
                t_avg = lines_y[i][3]
            else:
                x_avg = int(x / (i + 1 - j))
                y_avg = int(y / (i + 1 - j))
                z_avg = int(z / (i + 1 - j))
                t_avg = int(t / (i + 1 - j))
            lines_y1.append([x_avg, y_avg, z_avg, t_avg])
            j = i + 1
            y1 = lines_y[i+2][1]
    
    lines_y2 = []
    for i in range(0, len(lines_y1) - 1):
        if lines_y1[i+1][1] - lines_y1[i][1] < 50:
            lines_y2.append(lines_y1[i])
        if i == len(lines_y1) - 2:
            lines_y2.append(lines_y1[i + 1])
    lines_y1.clear()
    lines_y1 = lines_y2
    lines_y1.remove(lines_y1[0])
    lines_x.clear()
    lines_y.clear()
    lines_y = lines_y1
#     toạ độ nơi cắt
    lines_x.append(lines_x1[3])
    lines_x.append(lines_x1[4])
#     print(lines_x)
    
    lines_x1 = []
    for x in lines_x:
        lines_x1.append(x)
            
    lines_y1 = []
    for y in lines_y:
        if y[1] == y[3]:
            lines_y1.append([0, y[1], length_x, y[1]])
        else:
            
            a = (y[1] - y[3]) / (y[0] - y[2])
            b = (y[0] * y[3] - y[1] * y[2]) / (y[0] - y[2])
            lines_y1.append([0, int(b), length_x, int(a*length_x + b)])
    
    for x in lines_x1:
        cv2.line(input_img, (x[0],x[1]), (x[2],x[3]), (0,0,255), 1)
    for y in lines_y1:
        cv2.line(input_img, (y[0],y[1]), (y[2], y[3]), (0,0,255), 1)
        
    cv2.imwrite('/Users/nguyenviet/Desktop/CNN_DA/upload/temp_msv/msv.jpg',input_img)
    
    coordinate1 = []
    coordinate2 = []
    i = 0
    for x in lines_x:
        if x[0] == x[2]:
            x1 = x[0]
            for y in lines_y:
                if y[1] == y[3]:
                    y1 = y[1]
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
                else:
                    a1 = (y[1] - y[3]) / (y[0] - y[2])
                    b1 = (y[0] * y[3] - y[1] * y[2]) / (y[0] - y[2])
                    y1 = int(a1 * x1 + b1)
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
        else:
            for y in lines_y:
                if y[1] == y[3]:
                    y1 = y[1]
                    a1 = (x[1] - x[3]) / (x[0] - x[2])
                    b1 = (x[0] * x[3] - x[1] * x[2]) / (x[0] - x[2])
                    x1 = int((y1 - b1) / a1)
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
                else:
                    a1 = (x[1] - x[3]) / (x[0] - x[2])
                    b1 = (x[0] * x[3] - x[1] * x[2]) / (x[0] - x[2])
                    a2 = (y[1] - y[3]) / (y[0] - y[2])
                    b2 = (y[0] * y[3] - y[1] * y[2]) / (y[0] - y[2])
                    x1 = int((b2 - b1) / (a1 - a2))
                    y1 = int((a1 * b2 - a2 * b1) / (a1 - a2))
                    xy = []
                    xy.append(x1)
                    xy.append(y1)
                    if i == 0:
                        coordinate1.append(xy)
                    else:
                        coordinate2.append(xy)
        i += 1
    
    for y in reversed(coordinate1):
        coordinate1.remove(y)
        break
    for x in coordinate2:
        coordinate2.remove(x)
        break
    for x in coordinate1:
        t = x[1] + 2
        x[1] = t
    for x in coordinate2:
        t = x[1] + 2
        x[1] = t
        
    coordinates = []
    i = 0
    for x in coordinate1:
        coor = []
        coor.append(x[0])
        coor.append(x[1])
        j = 0
        for y in coordinate2:
            if j == i:
                coor.append(y[0])
                coor.append(y[1])
            j += 1
        i += 1
        coordinates.append(coor)
        
    return coordinates 



def getMaLopThi(coordinates):
    ix_1 = []
    j = 0
    input_img = Image.open('/Users/nguyenviet/Desktop/CNN_DA/upload/input/pic3.jpg').convert('L')
    for x in coordinates:
 
            # for debug
        # =============================================================================
        #     if stt != 15:
        #         stt += 1
        #         continue
        # =============================================================================
        x_lopthi = []
        u = 0
        if j == 0:
            for x1 in x:
                if u == 0:
                    x_lopthi.append(x1)
                else:
                    if u == 1:
                        x_lopthi.append((0+x[1]/2)+15)
                    else:
                        if u == 2:
                            x_lopthi.append((x[0]+x[2])/2)
                        else:
                            x_lopthi.append(x[1]-35)
                u += 1
            img = input_img.crop(x_lopthi)
            pytesseract.pytesseract.tesseract_cmd = (r'/usr/local/bin/tesseract')
            malop = pytesseract.image_to_string(img)
            ma_lop = ''
            for c in malop:
                try: 
                    ma_lop += str(int(c))
                except ValueError:
                    print('Nooooooo')
            digit1_location = '/Users/nguyenviet/Desktop/CnnDigitRecognize/temp/digit51.jpg'
            mi.imsave(digit1_location, img)
        j += 1        
            
    return ma_lop


def recognizeDigit(cnnModel, digit_location):
    digit_img = Image.open(digit_location).convert('L')
    digit_arr = np.asarray(digit_img)
    digit_arr.setflags(write=1)
    digit_arr = cv2.resize(digit_arr, (28, 28))
    digit_arr[0] = 0
    digit_arr[1] = 0
    digit_arr[2] = 0
    for x in digit_arr:
        i = 0
        for y in x:
            if y < 30:
                x[i] = 0
            i += 1
    
    # img = np.zeros((20,20,3), np.uint8)
    # mi.imsave('test.jpg', test)
    
    digit_arr = digit_arr / 255.0
    digit_arr = digit_arr.reshape(-1,28,28,1)
    # predict results
    results = cnnModel.predict(digit_arr)
    # select the indix with the maximum probability
    accuracy  = np.amax(results,axis = 1)
    results = np.argmax(results,axis = 1)
    ketqua = [results[0], accuracy[0]]
    # results = pd.Series(result,name="Label")
    return ketqua

def getDiem(coordinates):
    list_stt = []
    list_diem = []
    list_accuracy = []
    list_check = []
    digit_name = 'pic1'
    stt = 1
    model = reStoreModel()
    input_img = Image.open('/Users/nguyenviet/Desktop/CNN_DA/upload/input/pic1.jpg').convert('L')
    for x in coordinates:
            
        img = input_img.crop(x)
        check = np.asarray(img)
        check.setflags(write=1)
        for x in check:
            i = 0
            for y in x:
                t = 255 - y
                x[i] = t
                i += 1
        for x in check:
            i = 0
            for y in x:
                if y < 40:
                    x[i] = 0
                i += 1
        for x in check.T:
            check_weigh = len(x)
            break
        i = 0
        j = 0
        k = False
        for x in check:
            if i == int(check_weigh/2):
                for y in x:
                    if j < 10 or j > len(x) - 10:
                        j += 1
                        continue
                    if y == 0:
                        k = False
                    else: 
                        k = True
                        break
                    j += 1
            i += 1
        if k == False:
            digit1 = [0, 1.0]
            digit2 = [0, 1.0]
        else:
                # get and recognize digit
            getDigit1(img, digit_name, stt)
            getDigit2(img, digit_name, stt)
            digit1_location = '/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/digit1.jpg'
            digit2_location = '/Users/nguyenviet/Desktop/CNN_DA/upload/temp_diem/digit2.jpg'
            digit1 = recognizeDigit(model, digit1_location)
            digit2 = recognizeDigit(model, digit2_location)
            if digit2[0] != 5:
                digit2[0] = 0
        list_stt.append(stt)
        list_diem.append(str(str(digit1[0]) + ',' + str(digit2[0])))
        list_accuracy.append(str(round(digit1[1], 4)))
        if digit1[1] < 0.5:
            list_check.append('check')
        else:
            list_check.append('')
        # sh.write(stt, 0, stt)
        # sh.write(stt, 1, str(str(digit1[0]) + ',' + str(digit2[0])))
        # sh.write(stt, 2, str(round(digit1[1], 4)))
        # if digit1[1] < 0.5:
        #     sh.write(stt, 3, 'check')
            # print("Ket qua: %s,%s (%s)" %(digit1[0], digit2[0], digit1[1]))
        stt += 1
    ketqua = [list_stt, list_diem, list_accuracy, list_check]
    return ketqua

def getMSSV(coordinates):
    list_msv = []
    digit_name = 'pic2'
    stt = 1
    model = reStoreModel()
    input_img = Image.open('/Users/nguyenviet/Desktop/CNN_DA/upload/input/pic2.jpg').convert('L')
    for x in coordinates:
        img = input_img.crop(x)
        pytesseract.pytesseract.tesseract_cmd = (r'/usr/local/bin/tesseract')
        list_msv.append(pytesseract.image_to_string(img)) 
    return list_msv
