import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pickle
from pylab import rcParams
rcParams['figure.figsize'] = 10, 5

df=pd.read_csv('C:/Users/Admin/Desktop/black-friday/BlackFriday.csv')

df['Product_Category_2'] = df['Product_Category_2'].fillna(0)
df['Product_Category_3'] = df['Product_Category_3'].fillna(0)

#chuyển Product Category 2 và 3 về dạng số nguyên
df['Product_Category_2']=df['Product_Category_2'].astype(int)
df['Product_Category_3']=df['Product_Category_3'].astype(int)

#xóa trường Product ID và User ID
data=df.drop(['Product_ID','User_ID'],axis=1)

#đánh nhãn cho các trường
data['Gender']=data['Gender'].map( {'M': 0, 'F': 1} ).astype(int)
data['City_Category']=data['City_Category'].map( {'A': 0, 'B': 1, 'C':2} ).astype(int)
data['Age']=data['Age'].map( {'0-17': 0, '18-25': 1, '26-35': 2,'36-45':3,'46-50':4,
                         '51-55':5,'55+':6} ).astype(int)
data['Stay_In_Current_City_Years']=data['Stay_In_Current_City_Years'].map( {'0': 0, '1': 1, '2': 2,'3':3,'4+':4}).astype(int)

#Đưa về mảng cho X và y
X=data.drop(['Purchase'],axis=1).values
y=data['Purchase'].values

#Chọn trường có điểm cao nhất làm thuộc tính hồi qui
from sklearn.feature_selection import SelectPercentile
from sklearn.feature_selection import f_regression
Selector_f = SelectPercentile(f_regression, percentile=25)
Selector_f.fit(X,y)

#Hiển thị f_scores 
name_score=list(zip(data.drop(['Purchase'],axis=1).columns.tolist(),Selector_f.scores_))
name_score_df=pd.DataFrame(data=name_score,columns=['Feat_names','F_scores'])
name_score_df.sort_values('F_scores',ascending=False)

data=df.copy()
data=data[['Gender','City_Category','Product_Category_1', 
       'Product_Category_3','Purchase']]

#One-Hot Encoding
data=pd.get_dummies(data=data,columns=['Gender','City_Category','Product_Category_1','Product_Category_3'])

#Avoid dummy variable trap by removing one category of each categorical feature after encoding but before training
data.drop(['Gender_M','City_Category_A','Product_Category_1_1','Product_Category_3_0',],axis=1,inplace=True)

X=data.drop(['Purchase'],axis=1)
y=data['Purchase']
from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=0)
print("Training feature set size:",X_train.shape)
print("Test feature set size:",X_test.shape)
print("Training variable set size:",y_train.shape)
print("Test variable set size:",y_test.shape)

#from sklearn.preprocessing import StandardScaler
#sc_X=StandardScaler()
#X_train=sc_X.fit_transform(X_train)
#X_test=sc_X.transform(X_test)

from sklearn.linear_model import LinearRegression
regressor=LinearRegression()
regressor.fit(X_train,y_train)

y_pred=regressor.predict(X_test)

pickle.dump(regressor, open('model.pkl','wb'))

model = pickle.load(open('model.pkl','rb'))
