from PIL import Image
import numpy as np
import io
import os
import cv2 
import random
from math import log
import string
import random
import matplotlib.pyplot as plt
from flask import Response
from matplotlib.pyplot import imshow


def getImageMatrix(imageName):
    im = Image.open(imageName) 
    pix = im.load()
    color = 1
    if type(pix[0,0]) == int:
      color = 0
    image_size = im.size 
    image_matrix = []
    for width in range(int(image_size[0])):
        row = []
        for height in range(int(image_size[1])):
                row.append((pix[width,height]))
        image_matrix.append(row)
    return image_matrix,image_size[0],color

def ArnoldCatTransform(img, num):
    rows, cols, ch = img.shape
    n = rows
    img_arnold = np.zeros([rows, cols, ch])
    for x in range(0, rows):
        for y in range(0, cols):
            img_arnold[x][y] = img[(x+y)%n][(x+2*y)%n]  
    return img_arnold  

def ArnoldCatEncryption(imageName, key, password, constControl):

    imgName, img = getEncryptImageCatmap(imageName, int(key))
    imgNameEncryptXOR, imgEncryptXOR = encyptionImageWithXOR(imgName, img, password, float(constControl))
    return img, imgName, imgEncryptXOR, imgNameEncryptXOR

def getEncryptImageCatmap(imageName, key):

    img = cv2.imread(imageName)
    for i in range (0,key):
        img = ArnoldCatTransform(img, i)
    imgName = imageName.split('.')[0] + id_generator(8) + "_ArnoldcatEnc.png"
    cv2.imwrite(imgName, img)
    return imgName, img

def encyptionImageWithXOR(imageName, img, password, constControl):
    new_arr = []
    LogisticEncryptionIm = []
    _, _, color = getImageMatrix(imageName)
    dimension, dimension, _ = img.shape
    imageMatrix = cv2.imread(imageName)
    arr_2d, arr_2d_2, dimension, color = getKeyAndDimension(imageName, password, constControl)
    for i in range(dimension):
        row = []
        for j in range(dimension):
            row.append((imageMatrix[i][j] ^ arr_2d[i][j] ^ arr_2d_2[i][j])%256) 
        
        LogisticEncryptionIm.append(row)
    im = Image.new("L", (dimension, dimension))
    if color:
        im = Image.new("RGB", (dimension, dimension))
    else: 
        im = Image.new("L", (dimension, dimension)) # L is for Black and white pixels

    LogisticEncryptionIm = np.reshape(LogisticEncryptionIm, (dimension, dimension, 3))
    print(LogisticEncryptionIm)
    im = Image.fromarray(np.uint8(LogisticEncryptionIm))
    imgName = imageName.split('.')[0] + "_logistic.png"
    im.save(imgName, "PNG")
    return imgName, im

def ArnoldCatDecryption(imageName, key, password, constControl):
    
    imgNameDecLogistic, imgDecLogistic = decyptionLogisticWithXOR(imageName, password, float(constControl))
    imgNameArnold, imgArnold = decryptArnoldWithKey(imgNameDecLogistic, int(key))
    return imgNameDecLogistic, imgDecLogistic, imgNameArnold, imgArnold

def decyptionLogisticWithXOR(imgName, password, constControl):

    image = cv2.imread(imgName)
    LogisticDecryptionIm = []
    _, dimension, color = getImageMatrix(imgName)
    LogisticEncryptionIm = np.reshape(image, (dimension, dimension, 3))
    arr_2d, arr_2d_2, dimension, color = getKeyAndDimension(imgName, password, constControl)

    for i in range(dimension):
        row = []
        for j in range(dimension):
            row.append((LogisticEncryptionIm[i][j] ^ arr_2d[i][j] ^ arr_2d_2[i][j])%256)
        LogisticDecryptionIm.append(row)
    img = Image.new("L", (dimension, dimension))
    if color:
        img = Image.new("RGB", (dimension, dimension))
    else: 
        img = Image.new("L", (dimension, dimension)) # L is for Black and white pixels
    LogisticDecryptionIm = np.reshape(LogisticDecryptionIm, (dimension, dimension, 3))
    img = Image.fromarray(np.uint8(LogisticDecryptionIm))

    imgName = imgName.split('_')[0] +id_generator(8) + "_ArnoldcatDec_Logistic.png"
    img.save(imgName, "PNG")
    return imgName, img

def decryptArnoldWithKey(imgName, key):
    image = cv2.imread(imgName)
    dimension, dimension, ch = image.shape
    for k in range(0, key):
        image=arnold_cat_de(image)
    imgName = imgName.split('_')[0] + "_ArnoldcatDec.png"
    LogisticDecryptionIm = np.reshape(image, (dimension, dimension, 3))
    img = Image.fromarray(np.uint8(LogisticDecryptionIm))
    img.save(imgName, "PNG")
    return imgName, img

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def read_image(path):
	'''
	read image and catch any execeptions
	'''
	if os.path.isfile(path)==False:
		raise Exception("File not found or the program doesn't have required permissions")

	try:
		img = cv2.imread(path)
	except Exception as e:
		raise e
	else:
		return img

def logistic_key(x, r, size):

    key = []

    for i in range(size):   
        x = r*x*(1-x)   # The logistic equation
        key.append(int((x*pow(10, 16))%256))    # Converting the generated number between 0 to 255

    return key

def arnold_cat_de(img):
	'''
	inverse of arnold_cat_en function 
	'''
	h,w,_=img.shape
	decrypted_image = np.zeros([h,w,3])
	#rectangular images are not allowed 
	if h!=w:
		raise Exception("Expected a square image")

	for x in range(h):
		for y in range(h):
			decrypted_image[x][y]=img[(2*x-y)%h][(y-x)%h]

	img=decrypted_image
	return decrypted_image

def get_x_0(password):
    key_list = [ord(x) for x in password]
    S_i = 0.0
    T = 0.0
    for index in range(16):
        S_i = 0.123 + S_i
        T = (S_i*key_list[index] + T)
    T = T%256
    X_0 = T - int(T)
    return X_0

def getKeyAndDimension(imageName, password, constControl):
    img = cv2.imread(imageName)
    dimension, dimension, ch = img.shape
    
    pass1 = password[0:16]
    pass2 = password[16:32]

    x_0_1 = get_x_0(pass1)
    x_0_2 = get_x_0(pass2)

    key = logistic_key(x_0_1, constControl, dimension*dimension) 
    key2 = logistic_key(x_0_2, constControl, dimension*dimension) 
    arr_2d = np.reshape(key, (dimension, dimension))
    arr_2d_2 = np.reshape(key2, (dimension, dimension))

    _, _, color = getImageMatrix(imageName)
    return arr_2d, arr_2d_2, dimension, color

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
  return ''.join(random.choice(chars) for _ in range(size))

def create_plt(imageName):
    img = cv2.imread(imageName) 
    plt.switch_backend('Agg') 

    plt.figure(figsize=(14,6))
    histogram_blue = cv2.calcHist([img],[0],None,[256],[0,256])
    plt.plot(histogram_blue, color='blue') 
    histogram_green = cv2.calcHist([img],[1],None,[256],[0,256]) 
    plt.plot(histogram_green, color='green') 
    histogram_red = cv2.calcHist([img],[2],None,[256],[0,256]) 
    plt.plot(histogram_red, color='red')
    plt.title('Intensity Histogram - Original Image', fontsize=20)
    plt.xlabel('pixel values', fontsize=16)
    plt.ylabel('pixel count', fontsize=16) 
    imageName = imageName.split('_')[0] + id_generator(8) + "_histogram.png"

    plt.savefig(imageName)
    return imageName
